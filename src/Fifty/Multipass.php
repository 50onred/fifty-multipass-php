<?php namespace Fifty\Multipass;

class MultipassGenerator {
    protected static $block_size = 16;
    protected $site_key;
    protected $multipass_key;

    public function __construct($site_key, $multipass_key) {
        $this->site_key = $site_key;
        $this->multipass_key = $multipass_key;
    }

    public function getExpiration($now = 'now', $minutes = 5, $timezone = 'UTC', $format = \DateTime::ISO8601) {
        $now = new \DateTime($now, new \DateTimeZone($timezone));
        $now->add(new \DateInterval("PT{$minutes}M"));
        return $now->format($format);
    }

    public function generate($data) {
        $multipass = $this->getMultipass($data);
        $signature = $this->getSignature($multipass);
        return array($multipass, $signature);
    }

    protected function getSignature($multipass) {
        $signature = hash_hmac('sha1', $multipass, $this->multipass_key, true);
        return base64_encode($signature);
    }

    protected function getMultipass($data) {
        return base64_encode($this->encrypt($data));
    }

    protected function encrypt($data) {
        $iv = mcrypt_create_iv(self::$block_size, MCRYPT_DEV_URANDOM);
        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        mcrypt_generic_init($cipher, $this->getDigest(), $iv);
        $multipass = mcrypt_generic($cipher, $this->pad($data));
        mcrypt_generic_deinit($cipher);
        return $iv . $multipass;
    }

    protected function pad($data) {
        $json = json_encode($data);
        $pad = self::$block_size - (strlen($json) % self::$block_size);
        return $json . str_repeat(chr($pad), $pad);
    }

    protected function getDigest() {
        $digest = hash('sha1', $this->multipass_key . $this->site_key, true);
        return substr($digest, 0, self::$block_size);
    }
}
